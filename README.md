# An Extension of Mask-RCNN to Detect Hotdogs


## Pre-requisites ##
```
python 3.5+
virtualenv
postman
botframework emulator
```

## Instructions - Using existing model ##
### Setup ###
* Run in Terminal
```
git clone https://bitbucket.org/kainosrd/cv-workshop
cd cv-workshop
virtualenv -p python3 rcnn
pip3 install -r requirements.txt
sudo apt-get install python3-tk
```

### Run in Jupyter ###
* Run in Terminal `jupyter notebook`

### Run flask ###
* Run in terminal
```
python3 flaskdog.py
```

## Run Chatbot ##
* Run in terminal
```
cd mask-rcnn-chatbot
[sudo] npm install -g nodemon
npm install
nodemon app.js
```

## Instructions - Train new model ##
* Copy `hotdog.py` and create new class

* Create a new folder within `datasets` folder with the name of the dataset

* Add new folders `train` and `val` within the new folder

* Tag images using VIA tool `http://www.robots.ox.ac.uk/~vgg/software/via/via-1.0.6_demo.html` - need separate files for `train` and `val` data

* On a GPU powered computer, run in Terminal/CMD (using hotdog as an example)
`python3 hotdog.py train --dataset=datasets/hotdog --weights=coco`

* Takes approximately 2 hours on GPU, or over a week on a standard laptop